<?php
/*
Plugin Name: WP KML
Description: Plugin per la visualizzazione di tracciati KML e POI
Version: 0.1.1
Author: Enrico Lorenzoni, Stefano Furin
*/

/*
    Copyright (C) 2016  Dolomiti Project Srl

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


*/
defined( 'ABSPATH' ) or die( 'Come back later.' );

/// CONFIGURAZIONE

//$tileset_url = 'http://{s}.tile.opentopomap.org/{z}/{x}/{y}.png';
$tileset_url = 'https://{s}.tile.thunderforest.com/landscape/{z}/{x}/{y}.png?apikey=9194d8641dfe404283ebdb150e7b678e';

/// FINE CONFIGURAZIONE


/*

CHANGELOG

04/08/2016 - 0.1.0
    versione iniziale

08/08/2016 - 0.1.1
    aggunta di traduzioni
    aggiunta del tipo MIME kml e kmz per non richiedere modifica a wp-config
    titolo automatico se non inserito (kml e poi)
    coordinate a 0 se non inserite
    nessun errore se manca kml
    campo orari multiriga







*/

/***
 * Generate a thumbnail on the fly
 *
 * @return thumbnail url
 ***/
function get_thumb( $src_url = '', $width = null, $height = null, $crop = true, $cached = true ) {
	if ( empty( $src_url ) ) {
		throw new Exception( 'Invalid source URL' );
	}
	if ( empty( $width ) ) {
		$width = get_option( 'thumbnail_size_w' );
	}
	if ( empty( $height ) ) {
		$height = get_option( 'thumbnail_size_h' );
	}

	$src_info = pathinfo( $src_url );

	$upload_info = wp_upload_dir();

	$upload_dir = $upload_info['basedir'];
	$upload_url = $upload_info['baseurl'];
	$thumb_name = $src_info['filename'] . "_" . $width . "X" . $height . "." . $src_info['extension'];

	if ( false === strpos( $src_url, home_url() ) ) {
		$source_path = $upload_info['path'] . '/' . $src_info['basename'];
		$thumb_path  = $upload_info['path'] . '/' . $thumb_name;
		$thumb_url   = $upload_info['url'] . '/' . $thumb_name;
		if ( ! file_exists( $source_path ) && ! copy( $src_url, $source_path ) ) {
			throw new Exception( 'No permission on upload directory: ' . $upload_info['path'] );
		}
	} else {
		// define path of image
		$rel_path         = str_replace( $upload_url, '', $src_url );
		$source_path      = $upload_dir . $rel_path;
		$source_path_info = pathinfo( $source_path );
		$thumb_path       = $source_path_info['dirname'] . '/' . $thumb_name;

		$thumb_rel_path = str_replace( $upload_dir, '', $thumb_path );
		$thumb_url      = $upload_url . $thumb_rel_path;
	}

	if ( $cached && file_exists( $thumb_path ) ) {
		return $thumb_url;
	}

	$editor = wp_get_image_editor( $source_path );
	$editor->resize( $width, $height, $crop );
	$new_image_info = $editor->save( $thumb_path );

	if ( empty( $new_image_info ) ) {
		throw new Exception( 'Failed to create thumb: ' . $thumb_path );
	}

	return $thumb_url;
}

add_filter( 'upload_mimes', 'custom_upload_mimes' );
function custom_upload_mimes( $existing_mimes = array() ) {
	$existing_mimes['kml'] = 'application/vnd.google-earth.kml+xml';
	$existing_mimes['kmz'] = 'application/vnd.google-earth.kmz';

	return $existing_mimes;
}

load_plugin_textdomain( 'wpkml', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

add_action( 'init', 'create_post_type_kml_and_poi' );

function create_post_type_kml_and_poi() {
	register_post_type( 'kml', [
		'labels'      => [
			'name'          => __( 'Tracciati', 'wpkml' ),
			'singular_name' => __( 'Tracciato', 'wpkml' ),
		],
		'description' => __( 'Tracciati in formato KML', 'wpkml' ),
		'public'      => true,
		'has_archive' => true,
		'supports'    => [ 'title', 'editor' ],
	] );

	register_post_type( 'poi', [
		'labels'      => [
			'name'          => __( 'Punti di interesse', 'wpkml' ),
			'singular_name' => __( 'Punto di interesse', 'wpkml' ),
		],
		'description' => __( 'Punti di interesse', 'wpkml' ),
		'public'      => true,
		'has_archive' => true,
		'supports'    => [ 'title', 'editor' ],
	] );

	register_taxonomy( 'kmltype', 'kml', [
		'hierarchical' => true,
		'labels'       => [
			'name' => __( 'Tipi di tracciato', 'wpkml' ),
		],
	] );

	register_taxonomy( 'poitype', 'poi', [
		'hierarchical' => true,
		'labels'       => [
			'name' => __( 'Tipi di POI', 'wpkml' ),
		],
	] );
}

add_action( 'poitype_add_form_fields', 'add_custom_tax_field_oncreate' );
add_action( 'poitype_edit_form_fields', 'add_custom_tax_field_onedit' );

function add_custom_tax_field_oncreate( $term ) {
	?>
    <b><i><?= __( "Salva la tassonomia e poi modificala per poter aggiungere l'icona.", 'wpkml' ) ?></i></b>
    <br/><br/>
	<?php

}

function add_custom_tax_field_onedit( $term ) {
	?>
    <tr class="form-field form-required term-name-wrap">
        <th scope="row"><label for="name"><?= __( 'Icona', 'wpkml' ) ?></label></th>
        <td><input type='file' name='taxIcon' id='taxIcon'/>
            <p class="description"><?= __( 'Icona in formato PNG o JPG. Dimensioni consigliate: 25x41px.', 'wpkml' ) ?>
				<?php

				$id  = $term->term_id;
				$img = get_term_meta( $id, 'icon', true );
				if ( is_array( $img ) ) {
					?>
                    <br/>
					<?= __( 'Immagine caricata:', 'wpkml' ) ?>
                    <img src='<?= $img['url'] ?>'/>
					<?php

				} else {
					?>
                    <br/><?= __( "Nessuna immagine caricata: verrà usata l'icona di default.", 'wpkml' ) ?>
					<?php

				}

				?>
            </p></td>
    </tr>
	<?php

}

add_action( 'create_poitype', 'save_custom_tax_field' );
add_action( 'edited_poitype', 'save_custom_tax_field' );

function save_custom_tax_field( $termID ) {

	/// IMMAGINE ICONA
	if ( ! empty( $_FILES['taxIcon']['name'] ) ) {
		if ( $_FILES['taxIcon']['type'] == 'image/png' || $_FILES['taxIcon']['type'] == 'image/jpeg' ) {
			$upload = wp_upload_bits( $_FILES['taxIcon']['name'], null, file_get_contents( $_FILES['taxIcon']['tmp_name'] ) );

			if ( isset( $upload['error'] ) && $upload['error'] != '' ) {
				wp_die( __( "Si è verificato un errore durante l'upload:", 'wpkml' ) . ' ' . $upload['error'] );
			} else {
				update_term_meta( $termID, 'icon', $upload );
			}
		} else {
			wp_die( __( "Il file caricato non sembra un'immagine valida", 'wpkml' ) . ' (' . $_FILES['taxIcon']['type'] . ')' );
		}
	}
}

function edit_form_tag() {
	echo ' enctype="multipart/form-data"';
}

add_action( 'poitype_term_edit_form_tag', 'edit_form_tag' );

// supporto ad allegati nei post
add_action( 'post_edit_form_tag', 'update_edit_form' );
function update_edit_form() {
	echo ' enctype="multipart/form-data"';
}

/// META BOX

add_action( 'add_meta_boxes', 'add_custom_meta_boxes' );
function add_custom_meta_boxes() {
	$context = 'side';
	add_meta_box( 'kml_attachment', 'File GeoJSON', 'kml_attachment', 'kml', 'normal', 'low' );
	add_meta_box( 'kml_trails', 'N. segnavia', 'kml_trails', 'kml', $context, 'low' );
	add_meta_box( 'kml_difficulty', 'Difficoltà', 'kml_difficulty', 'kml', $context, 'low' );
	add_meta_box( 'kml_length', 'Lunghezza totale (km)', 'kml_length', 'kml', $context, 'low' );
	add_meta_box( 'kml_hdifference', 'Dislivello (m)', 'kml_hdifference', 'kml', $context, 'low' );
	add_meta_box( 'kml_hmin', 'Quota minima (m)', 'kml_hmin', 'kml', $context, 'low' );
	add_meta_box( 'kml_hmax', 'Quota massima (m)', 'kml_hmax', 'kml', $context, 'low' );
	add_meta_box( 'kml_fondo', 'Tipo di fondo', 'kml_fondo', 'kml', $context, 'low' );
	add_meta_box( 'kml_percorrenza', 'Tempo di percorrenza (cicloturista)', 'kml_percorrenza', 'kml', $context, 'low' );
	add_meta_box( 'kml_percorrenza2', 'Tempo di percorrenza (amatore)', 'kml_percorrenza2', 'kml', $context, 'low' );
	add_meta_box( 'kml_pictures', 'Foto', 'kml_pictures', 'kml', 'normal', 'low' );
	add_meta_box( 'kml_hpicture', 'Immagine altimetria (png o jpg)', 'kml_hpicture', 'kml', 'normal', 'low' );
	add_meta_box( 'kml_featured', 'Immagine in evidenza (png o jpg)', 'kml_featured', 'kml', 'normal', 'low' );
	add_meta_box( 'kml_kml', 'File KML', 'kml_kml', 'kml', 'normal', 'low' );
	add_meta_box( 'kml_gpx', 'File GPX', 'kml_gpx', 'kml', 'normal', 'low' );
	add_meta_box( 'kml_scheda', 'scheda', 'kml_scheda', 'kml', 'normal', 'low' );

	add_meta_box( 'poi_coordinates', 'Coordinate', 'poi_coordinates', 'poi', 'normal', 'low' );
	add_meta_box( 'poi_picture', 'Foto', 'poi_picture', 'poi', 'normal', 'low' );
	add_meta_box( 'poi_times', 'Orari', 'poi_times', 'poi', $context, 'low' );
	add_meta_box( 'poi_website', 'Sito web', 'poi_website', 'poi', $context, 'low' );
	add_meta_box( 'poi_phone', 'Telefono', 'poi_phone', 'poi', $context, 'low' );
	add_meta_box( 'poi_email', 'E-mail', 'poi_email', 'poi', $context, 'low' );
	add_meta_box( 'poi_directlink', 'Link diretto', 'poi_directlink', 'poi', 'normal', 'low' );
}

function poi_coordinates() {
	$lat = htmlentities( get_post_meta( get_the_ID(), 'lat', true ) );
	$lng = htmlentities( get_post_meta( get_the_ID(), 'lng', true ) );

	?>
    <i><?= __( 'Campo obbligatorio. Formato decimale (es: 45.0000 11.0000)', 'wpkml' ) ?></i><br/><br/>
    <table>
        <tr>
            <td style="text-align: right;"><?= __( 'Latitudine:', 'wpkml' ) ?></td>
            <td><input value="<?= $lat ?>" name="lat"/></td>
        </tr>
        <tr>
            <td style="text-align: right;"><?= __( 'Longitudine:', 'wpkml' ) ?></td>
            <td><input value="<?= $lng ?>" name="lng"/></td>
        </tr>
    </table>
	<?php

	//$title = get_the_title();

	/*
	if ($title == "" || $lat == "" || $lng == "") {
		?>
		<span style="color: red">
		<?=__("Ricorda di compilare il titolo e le coordinate per poter pubblicare il post.", 'wpkml') ?>
		</span>
		<?
	}
	*/
}

function poi_picture() {
	wp_nonce_field( plugin_basename( __FILE__ ), 'poi_picture_nonce' );

	?>
	<?= __( 'Foto del punto di interesse (png o jpg):', 'wpkml' ) ?><br/>

	<?php

	$poi_picture = get_post_meta( get_the_ID(), 'poi_picture', true );

	if ( $poi_picture != '' ) {
		?>
        <span style="color: forestGreen">
        <?= __( 'File caricato:', 'wpkml' ) ?> <?= $poi_picture['file'] ?><br/>
        <img src='<?= $poi_picture['url'] ?>' style="width: 200px;"/>
        </span>
		<?php

	} else {
		?>
        <span style="color: orange">
        <?= __( 'File assente', 'wpkml' ) ?>
        </span>
		<?php

	}
	?>
    <br/><input type="file" name="poi_picture"/>

	<?php

}

function poi_times() {
	$poi_times = get_post_meta( get_the_ID(), 'poi_times', true );
	?>
    <textarea rows="8" name="poi_times"><?= $poi_times ?></textarea>
	<?php

}

function poi_website() {
	$poi_website = get_post_meta( get_the_ID(), 'poi_website', true );
	?>
    <input value="<?= $poi_website ?>" name="poi_website"/>
	<?php

}

function poi_phone() {
	$poi_phone = get_post_meta( get_the_ID(), 'poi_phone', true );
	?>
    <input value="<?= $poi_phone ?>" name="poi_phone"/>
	<?php

}

function poi_email() {
	$poi_email = get_post_meta( get_the_ID(), 'poi_email', true );
	?>
    <input value="<?= $poi_email ?>" name="poi_email"/>
	<?php

}

function poi_directlink() {
	$poi_directlink = get_post_meta( get_the_ID(), 'poi_directlink', true );
	?>
    <i><?= __( 'Attenzione: la presenza di un link diretto causerà un redirect 302 invece di visualizzare la pagina. Formati validi: http://www.google.it https://www.google.it www.google.it', 'wpkml' ) ?></i>
    <br/>
    <input value="<?= $poi_directlink ?>" name="poi_directlink"/>
	<?php

	if ( $poi_directlink != '' ) {
		?>
        <br/>
        <span style="color: red">
        <?= __( 'Redirect attivo', 'wpkml' ) ?>
        </span>
		<?php

	}
}

function kml_attachment() {
	global $post;
	wp_nonce_field( plugin_basename( __FILE__ ), 'kml_attachment_nonce' );
	?><p class="description">
        Carica il file GeoJSON della traccia da visualizzare (per convertire da altri formati visitare <a
                href="http://2geojson.com/index.php" target="_blank">questo servizio online</a> e utilizzare i valori di
        default).
    </p>
	<?php

	$custom         = get_post_custom( $post->ID );
	$kml_attachment = $custom['kml_attachment'][0];

	if ( $kml_attachment != '' ) {
		$attachment_array = unserialize( $kml_attachment );
		?>
        <span style="color: forestGreen">
        File caricato: <?= $attachment_array['file'] ?>
        </span>
		<?php

	} else {
		/*
		?>
		<span style="color: red">
		Ricorda di caricare un file KML per poter pubblicare il post.
		</span>
		<?
		*/
	}

	?>
    <br/>
	<?php
	if ( $post->post_title == '' ) {
		?><span style="color: red">
        Ricorda di assegnare un titolo al post per poterlo pubblicare.<br/>
        </span><?php

	}
	?>
    <input type="file" name="kml_attachment"/>
	<?php

}

function kml_trails() {
	global $post;
	$custom = get_post_custom( $post->ID );
	$trails = $custom['trails'][0];
	?>
    <input value="<?= $trails ?>" name="trails"/>
	<?php

}

function kml_difficulty() {
	global $post;
	$custom     = get_post_custom( $post->ID );
	$difficulty = $custom['difficulty'][0];
	?><input value="<?= $difficulty ?>" name="difficulty" /><?php

}

function kml_length() {
	global $post;
	$custom = get_post_custom( $post->ID );
	$length = $custom['length'][0];
	?><input value="<?= $length ?>" name="length" /><?php

}

function kml_hdifference() {
	global $post;
	$custom      = get_post_custom( $post->ID );
	$hdifference = $custom['hdifference'][0];
	?><input value="<?= $hdifference ?>" name="hdifference" /><?php

}

function kml_hmin() {
	global $post;
	$custom = get_post_custom( $post->ID );
	$hmin   = $custom['hmin'][0];
	?><input value="<?= $hmin ?>" name="hmin" /><?php

}

function kml_hmax() {
	global $post;
	$custom = get_post_custom( $post->ID );
	$hmax   = $custom['hmax'][0];
	?><input value="<?= $hmax ?>" name="hmax" /><?php

}


function kml_fondo() {
	global $post;
	$custom = get_post_custom( $post->ID );
	$fondo  = $custom['fondo'][0];
	?><input value="<?= $fondo ?>" name="fondo" /><?php

}


function kml_percorrenza() {
	global $post;
	$custom      = get_post_custom( $post->ID );
	$percorrenza = $custom['percorrenza'][0];
	?><input value="<?= $percorrenza ?>" name="percorrenza" /><?php

}

function kml_percorrenza2() {
	global $post;
	$custom       = get_post_custom( $post->ID );
	$percorrenza2 = $custom['percorrenza2'][0];
	?><input value="<?= $percorrenza2 ?>" name="percorrenza2" /><?php

}


function kml_pictures() {
	?>
    Carica fotografie:<br/>
    <i>Puoi caricare fino a 5 file per volta.</i>
    <br/><input type="file" name="kml_pictures[]"/>
    <br/><input type="file" name="kml_pictures[]"/>
    <br/><input type="file" name="kml_pictures[]"/>
    <br/><input type="file" name="kml_pictures[]"/>
    <br/><input type="file" name="kml_pictures[]"/>
    <br/><br/>
    <style>
        .imagestable {
            width: 100%;
        }

        .imagestable td {
            border: 0.5px solid #eee;
            text-align: center;
        }
    </style>
    <table class="imagestable">
		<?php

		$data = get_post_meta( get_the_ID(), 'kml_pictures', true );

		if ( is_array( $data ) ) {
			for ( $i = 0; $i < count( $data ); $i ++ ) {
				?>

                <tr>
                    <td><?= $i ?></td>
                    <td><?php if ( file_exists( $data[ $i ]['file'] ) ) {
							?><img src='<?= $data[ $i ]['url'] ?>' style="width: 200px;" /><?php

						} else {
							?>File non esistente<?php

						}
						?></td>
                    <td><?= basename( $data[ $i ]['file'] ) ?></td>
                    <td><input type="checkbox" name="kml_pictures_delete_<?= $i ?>"/>Elimina</td>
                </tr>


				<?php

			}
		}

		?>
    </table>
	<?php

}

function kml_hpicture() {
	?>
    Immagine dell'altimetria (png o jpg):<br/>

	<?php

	$custom       = get_post_custom( $post->ID );
	$kml_hpicture = $custom['kml_hpicture'][0];

	if ( $kml_hpicture != '' ) {
		$attachment_array = unserialize( $kml_hpicture );
		?>
        <span style="color: forestGreen">
        File caricato: <?= $attachment_array['file'] ?><br/>
        <img src='<?= $attachment_array['url'] ?>' style="width: 200px;"/>
        </span>
		<?php

	} else {
		?>
        <span style="color: orange">
        File assente
        </span>
		<?php

	}
	?>
    <br/><input type="file" name="kml_hpicture"/>

	<?php

}

function kml_featured() {
	?>
    Immagine dell'altimetria (png o jpg):<br/>

	<?php

	$custom       = get_post_custom( $post->ID );
	$kml_featured = $custom['kml_featured'][0];

	if ( $kml_featured != '' ) {
		$attachment_array = unserialize( $kml_featured );
		?>
        <span style="color: forestGreen">
        File caricato: <?= $attachment_array['file'] ?><br/>
        <img src='<?= $attachment_array['url'] ?>' style="width: 200px;"/>
        </span>
        <td><input type="checkbox" name="kml_featured_delete"/>Svuota</td>
		<?php

	} else {
		?>
        <span style="color: orange">
        File assente
        </span>
		<?php

	}
	?>
    <br/><input type="file" name="kml_featured"/>

	<?php

}

function kml_kml() {
	?>
    File KML per il download:<br/>

	<?php

	$custom  = get_post_custom( $post->ID );
	$kml_kml = $custom['kml_kml'][0];

	if ( $kml_kml != '' ) {
		$attachment_array = unserialize( $kml_kml );
		?>
        <span style="color: forestGreen">
        File caricato: <?= $attachment_array['file'] ?><br/>
        </span>
		<?php

	} else {
		?>
        <span style="color: orange">
        File assente
        </span>
		<?php

	}
	?>
    <br/><input type="file" name="kml_kml"/>

	<?php

}

function kml_gpx() {
	?>
    File GPX per il download:<br/>

	<?php

	$custom  = get_post_custom( $post->ID );
	$kml_gpx = $custom['kml_gpx'][0];

	if ( $kml_gpx != '' ) {
		$attachment_array = unserialize( $kml_gpx );
		?>
        <span style="color: forestGreen">
        File caricato: <?= $attachment_array['file'] ?><br/>
        </span>
		<?php

	} else {
		?>
        <span style="color: orange">
        File assente
        </span>
		<?php

	}
	?>
    <br/><input type="file" name="kml_gpx"/>

	<?php

}
function kml_scheda() {
	?>
    File PDF per il download:<br/>

	<?php

	$custom  = get_post_custom( $post->ID );
	$kml_scheda = $custom['kml_scheda'][0];

	if ( $kml_scheda != '' ) {
		$attachment_array = unserialize( $kml_scheda );
		?>
        <span style="color: forestGreen">
        File caricato: <?= $attachment_array['file'] ?><br/>
        </span>
		<?php

	} else {
		?>
        <span style="color: orange">
        File assente
        </span>
		<?php

	}
	?>
    <br/><input type="file" name="kml_scheda"/>

	<?php

}


add_action( 'save_post', 'save_details' );

function save_details( $id ) {
	global $post;

	if ( $post->post_type == 'kml' ) {
		update_post_meta( $post->ID, 'trails', substr( $_POST['trails'], 0, 5 ) );
		update_post_meta( $post->ID, 'difficulty', substr( $_POST['difficulty'], 0, 15 ) );
		update_post_meta( $post->ID, 'length', intval( $_POST['length'] ) );
		update_post_meta( $post->ID, 'hdifference', intval( $_POST['hdifference'] ) );
		update_post_meta( $post->ID, 'hmin', intval( $_POST['hmin'] ) );
		update_post_meta( $post->ID, 'hmax', intval( $_POST['hmax'] ) );
		update_post_meta( $post->ID, 'fondo', substr( $_POST['fondo'], 0, 50 ) );
		update_post_meta( $post->ID, 'percorrenza', substr( $_POST['percorrenza'], 0, 25 ) );
		update_post_meta( $post->ID, 'percorrenza2', substr( $_POST['percorrenza2'], 0, 25 ) );

		/// gestione degli allegati

		if ( ! wp_verify_nonce( $_POST['kml_attachment_nonce'], plugin_basename( __FILE__ ) ) ) {
			return $id;
		}

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $id;
		}

		if ( ! empty( $_FILES['kml_attachment']['name'] ) ) {
			//if ($_FILES['kml_attachment']['type'] == 'application/vnd.google-earth.kml+xml' || $_FILES['kml_attachment']['type'] == 'application/vnd.google-earth.kmz') {
			if ( $_FILES['kml_attachment']['type'] == 'application/json' ) {
				$upload = wp_upload_bits( $_FILES['kml_attachment']['name'], null, file_get_contents( $_FILES['kml_attachment']['tmp_name'] ) );

				if ( isset( $upload['error'] ) && $upload['error'] != '' ) {
					wp_die( "Si è verificato un errore durante l'upload: " . $upload['error'] );
				} else {
					update_post_meta( $id, 'kml_attachment', $upload );
				}
			} else {
				wp_die( __( 'Il file caricato non sembra un GEOJSON... (' . $_FILES['kml_attachment']['type'] . ')', 'wpkml' ) );
			}
		}

		//// VERIFICA TITOLO E KML OBBLIGATORI
		$kml      = get_post_meta( get_the_ID(), 'kml_attachment', true );
		$newtitle = $_POST['post_title'];

		if ( $newtitle == '' ) {
			remove_action( 'save_post', 'save_details' );
			wp_update_post( array( 'ID' => $id, 'post_title' => 'Itinerario senza nome' . $id ) );
			add_action( 'save_post', 'save_details' );
		}

		/// IMMAGINE ALTIMETRIA
		if ( ! empty( $_FILES['kml_hpicture']['name'] ) ) {
			if ( $_FILES['kml_hpicture']['type'] == 'image/png' || $_FILES['kml_hpicture']['type'] == 'image/jpeg' ) {
				$upload = wp_upload_bits( $_FILES['kml_hpicture']['name'], null, file_get_contents( $_FILES['kml_hpicture']['tmp_name'] ) );

				if ( isset( $upload['error'] ) && $upload['error'] != '' ) {
					wp_die( "Si è verificato un errore durante l'upload: " . $upload['error'] );
				} else {
					update_post_meta( $id, 'kml_hpicture', $upload );
				}
			} else {
				wp_die( "Il file caricato non sembra un'immagine valida (" . $_FILES['kml_hpicture']['type'] . ')' );
			}
		}

		/// IMMAGINE IN EVIDENZA
		if ( ! empty( $_FILES['kml_featured']['name'] ) ) {
			if ( $_FILES['kml_featured']['type'] == 'image/png' || $_FILES['kml_featured']['type'] == 'image/jpeg' ) {
				$upload = wp_upload_bits( $_FILES['kml_featured']['name'], null, file_get_contents( $_FILES['kml_featured']['tmp_name'] ) );

				if ( isset( $upload['error'] ) && $upload['error'] != '' ) {
					wp_die( "Si è verificato un errore durante l'upload: " . $upload['error'] );
				} else {
					update_post_meta( $id, 'kml_featured', $upload );
				}
			} else {
				wp_die( "Il file caricato non sembra un'immagine valida (" . $_FILES['kml_featured']['type'] . ')' );
			}
		}

		/// FILE KML
		if ( ! empty( $_FILES['kml_kml']['name'] ) ) {
			$upload = wp_upload_bits( $_FILES['kml_kml']['name'], null, file_get_contents( $_FILES['kml_kml']['tmp_name'] ) );

			if ( isset( $upload['error'] ) && $upload['error'] != '' ) {
				wp_die( "Si è verificato un errore durante l'upload: " . $upload['error'] );
			} else {
				update_post_meta( $id, 'kml_kml', $upload );
			}

		}

		/// FILE GPX
		if ( ! empty( $_FILES['kml_gpx']['name'] ) ) {
			$upload = wp_upload_bits( $_FILES['kml_gpx']['name'], null, file_get_contents( $_FILES['kml_gpx']['tmp_name'] ) );

			if ( isset( $upload['error'] ) && $upload['error'] != '' ) {
				wp_die( "Si è verificato un errore durante l'upload: " . $upload['error'] );
			} else {
				update_post_meta( $id, 'kml_gpx', $upload );
			}

		}
		/// FILE SCHEDA
		if ( ! empty( $_FILES['kml_scheda']['name'] ) ) {
			$upload = wp_upload_bits( $_FILES['kml_scheda']['name'], null, file_get_contents( $_FILES['kml_scheda']['tmp_name'] ) );

			if ( isset( $upload['error'] ) && $upload['error'] != '' ) {
				wp_die( "Si è verificato un errore durante l'upload: " . $upload['error'] );
			} else {
				update_post_meta( $id, 'kml_scheda', $upload );
			}

		}

		// gestione eliminazione immagine
		$delete_ids = [];
		foreach ( array_keys( $_POST ) as $k ) {
			if ( substr( $k, 0, strlen( 'kml_pictures_delete_' ) ) == 'kml_pictures_delete_' ) {
				$picture_id_to_delete = substr( $k, strlen( 'kml_pictures_delete_' ) );
				$delete_ids[]         = $picture_id_to_delete;
			}
		}

		// gestione eliminazione immagine in evidenza
		if ( ! empty( $_POST['kml_featured_delete'] ) ) {
			delete_post_meta( $id, 'kml_featured' );
		}


		if ( count( $delete_ids ) > 0 ) {
			$oldimages = get_post_meta( get_the_ID(), 'kml_pictures', true );
			foreach ( $delete_ids as $di ) {
				unset( $oldimages[ $di ] );
			}
			$oldimages = array_values( $oldimages );
			update_post_meta( $id, 'kml_pictures', $oldimages );
		}

		/// upload foto
		for ( $i = 0; $i < count( $_FILES['kml_pictures']['name'] ); $i ++ ) {
			if ( ! empty( $_FILES['kml_pictures']['name'][ $i ] ) ) {
				if ( $_FILES['kml_pictures']['type'][ $i ] == 'image/png' || $_FILES['kml_pictures']['type'][ $i ] == 'image/jpeg' ) {
					$upload = wp_upload_bits( $_FILES['kml_pictures']['name'][ $i ], null, file_get_contents( $_FILES['kml_pictures']['tmp_name'][ $i ] ) );

					if ( isset( $upload['error'] ) && $upload['error'] != '' ) {
						wp_die( "Si è verificato un errore durante l'upload: " . $upload['error'] );
					} else {
						$oldimages = get_post_meta( get_the_ID(), 'kml_pictures', true );
						if ( ! is_array( $oldimages ) ) {
							$oldimages = [];
						}

						$oldimages[] = $upload;
						update_post_meta( $id, 'kml_pictures', $oldimages );
					}
				} else {
					wp_die( "Il file caricato non sembra un'immagine valida (" . $_FILES['kml_pictures']['type'][ $i ] . ')' );
				}
			}
		}
	}

	if ( $post->post_type == 'poi' ) {
		// salvataggio POI


		if ( ! wp_verify_nonce( $_POST['poi_picture_nonce'], plugin_basename( __FILE__ ) ) ) {
			return $id;
		}

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $id;
		}

		//// VERIFICA TITOLO E KML OBBLIGATORI
		$newtitle = $_POST['post_title'];
		$lat      = $_POST['lat'];
		$lng      = $_POST['lng'];

		if ( $newtitle == '' ) {
			remove_action( 'save_post', 'save_details' );
			wp_update_post( array( 'ID' => $id, 'post_title' => 'POI senza nome ' . $id ) );
			add_action( 'save_post', 'save_details' );
		}
		if ( $lat == '' ) {
			$_POST['lat'] = 0;
		}
		if ( $lng == '' ) {
			$_POST['lng'] = 0;
		}

		update_post_meta( $id, 'lat', floatval( $_POST['lat'] ) );
		update_post_meta( $id, 'lng', floatval( $_POST['lng'] ) );

		update_post_meta( $id, 'poi_times', $_POST['poi_times'] );
		update_post_meta( $id, 'poi_website', $_POST['poi_website'] );
		update_post_meta( $id, 'poi_phone', $_POST['poi_phone'] );
		update_post_meta( $id, 'poi_email', $_POST['poi_email'] );
		update_post_meta( $id, 'poi_directlink', $_POST['poi_directlink'] );

		if ( ! empty( $_FILES['poi_picture']['name'] ) ) {
			if ( $_FILES['poi_picture']['type'] == 'image/png' || $_FILES['poi_picture']['type'] == 'image/jpeg' ) {
				$upload = wp_upload_bits( $_FILES['poi_picture']['name'], null, file_get_contents( $_FILES['poi_picture']['tmp_name'] ) );

				if ( isset( $upload['error'] ) && $upload['error'] != '' ) {
					wp_die( "Si è verificato un errore durante l'upload: " . $upload['error'] );
				} else {
					update_post_meta( $id, 'poi_picture', $upload );
				}
			} else {
				wp_die( "Il file caricato non sembra un'immagine valida (" . $_FILES['poi_picture']['type'] . ')' );
			}
		}
	}
}

add_filter( 'the_content', 'show_kml_metatada' );

function show_kml_metatada( $content ) {
	global $tileset_url;
	$type = $GLOBALS['post']->post_type;
	?>
    <div class="row" style="margin-top: 40px;">

	<?php
	if ( ! is_archive() ) {
		if ( $type == 'kml' ) {

			///// VISUALIZZAZIONE KML

			$id       = $GLOBALS['post']->ID;
			$metadata = get_post_meta( $id );

			?>
            <div class="col-sm-12">
                <h2 id="page-title"><?php the_title(); ?></h2>
            </div>
            <div class="col-sm-12 col-md-8 col-lg-8">
                <div id="mapid"></div>
                <script>
                    var mymap = L.map('mapid').setView([44.8126604, 9.9683096], 13);

                    L.tileLayer('<?=$tileset_url ?>', {
                        attribution: 'Punto3',
                        maxZoom: 16,
                        minZoom: 8
                    }).addTo(mymap);
                    var geojson_stirone = new L.geoJson();

                    geojson_stirone.addTo(mymap);
                    $.ajax({
                        dataType: "json",
                        url: "<?php echo get_stylesheet_directory_uri(); ?>/geojson/stirone.json",
                        success: function (data) {
                            $(data.features).each(function (key, data) {
                                geojson_stirone.addData(data);
                            });
                            geojson_stirone.eachLayer(function (layer) {
                                layer.setStyle({
                                    weight: 0,
                                    opacity: .1,
                                    color: '#00b900',
                                    fillOpacity: 0.1,
                                    fillColor: '#00b900'
                                })
                            });
                        }
                    }).error(function () {
                    });

					<?php
					$kml_attachment = get_post_meta( get_the_ID(), 'kml_attachment', true );

					if (is_array( $kml_attachment )) {
					$jsonfile = parse_url( $kml_attachment['url'] );

					?>

                    var geojson_itinerario = new L.geoJson();
                    geojson_itinerario.addTo(mymap);

                    $.ajax({
                        dataType: "json",
                        url: "<?=$jsonfile['path'] ?>",
                        success: function (data) {
                            $(data.features).each(function (key, data) {
                                geojson_itinerario.addData(data);
                            });

                            mymap.fitBounds(geojson_itinerario.getBounds());
                        }
                    }).error(function () {
                    });

					<?php

					}
					?>


                    mymap.on('dragend', redrawPOIs);
                    mymap.on('zoomend', redrawPOIs);

                    loadedpois = [];
                    poi = [];

                    function redrawPOIs(arg) {
                        console.log(mymap.getBounds());

                        jQuery.post(
                            "<?=admin_url( 'admin-ajax.php' ) ?>",
                            {
                                'action': 'get_pois',
                                'data': {
                                    west: mymap.getBounds().getWest(),
                                    south: mymap.getBounds().getSouth(),
                                    east: mymap.getBounds().getEast(),
                                    north: mymap.getBounds().getNorth(),
                                    lang: "<?=ICL_LANGUAGE_CODE ?>",


                                }
                            },
                            function (response) {

                                res_array = JSON.parse(response);
                                console.log(res_array);
                                for (var i = 0; i < res_array.length; i++) {

                                    newpoi = res_array[i];
                                    if (loadedpois.indexOf(newpoi.id) > -1) {
                                        console.log("Già caricato..");
                                    } else {
                                        var poicon = newpoi.icon;
                                        if (poicon != "") {
                                            console.log("Icona custom");
                                            var myIcon = L.icon({
                                                iconUrl: poicon,
                                                //iconRetinaUrl: 'my-icon@2x.png',
                                                //iconSize: [25, 41],
                                                iconAnchor: [12, 41],
                                                //popupAnchor: [12, 0],
                                                //shadowUrl: 'my-icon-shadow.png',
                                                //shadowRetinaUrl: 'my-icon-shadow@2x.png',
                                                //shadowSize: [68, 95],
                                                //shadowAnchor: [22, 94]
                                            });
                                            var options = {icon: myIcon};
                                        } else {
                                            var options = {};
                                        }
                                        poi[newpoi.id] = L.marker([newpoi.lat, newpoi.lng], options).addTo(mymap);
                                        poi[newpoi.id].bindPopup("<a href='" + newpoi.permalink + "'><b>" + newpoi.title + "</b></a>");
                                        loadedpois.push(newpoi.id);
                                    }

                                }

                            }
                        );

                    }

                    redrawPOIs();


                </script>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4 kml--info">
                <h3 id="kml-metadata-title">
					<?= __( 'Caratteristiche del percorso:', 'wpkml' ) ?>
                </h3>
                <ul>
					<?php

					$valore = get_post_meta( get_the_ID(), 'trails', true );
					if ( $valore != '' ) {
						?>
                        <li class="kml-metadata-div"><b><?= __( 'N. segnavia', 'wpkml' ) ?></b>: <?= $valore ?></li>
						<?php
					}
					$valore = get_post_meta( get_the_ID(), 'difficulty', true );
					if ( $valore != '' ) {
						?>
                        <li class="kml-metadata-div"><b><?= __( 'Difficolt&agrave;', 'wpkml' ) ?></b>: <?= $valore ?>
                        </li>
						<?php
					}
					$valore = get_post_meta( get_the_ID(), 'length', true );
					if ( $valore != '' ) {
						?>
                        <li class="kml-metadata-div"><b><?= __( 'Lunghezza', 'wpkml' ) ?></b>: <?= $valore ?> km</li>
						<?php
					}
					$valore = get_post_meta( get_the_ID(), 'hdifference', true );
					if ( $valore != '' ) {
						?>
                        <li class="kml-metadata-div"><b><?= __( 'Dislivello', 'wpkml' ) ?></b>: <?= $valore ?> m</li>
						<?php
					}
					$valore = get_post_meta( get_the_ID(), 'hmin', true );
					if ( $valore != '' ) {
						?>
                        <li class="kml-metadata-div"><b><?= __( 'Altezza minima', 'wpkml' ) ?></b>: <?= $valore ?> m
                        </li>
						<?php
					}
					$valore = get_post_meta( get_the_ID(), 'hmax', true );
					if ( $valore != '' ) {
						?>
                        <li class="kml-metadata-div"><b><?= __( 'Altezza massima', 'wpkml' ) ?></b>: <?= $valore ?> m
                        </li>
						<?php
					}
					$valore = get_post_meta( get_the_ID(), 'fondo', true );
					if ( $valore != '' ) {
						?>
                        <li class="kml-metadata-div"><b><?= __( 'Fondo stradale', 'wpkml' ) ?></b>: <?= $valore ?></li>
						<?php
					}
					$valore = get_post_meta( get_the_ID(), 'percorrenza', true );
					if ( $valore != '' ) {
						?>
                        <li class="kml-metadata-div">
                            <b><?= __( 'Tempi di percorrenza (cicloturista)', 'wpkml' ) ?></b>: <?= $valore ?></li>
						<?php
					}

					$valore = get_post_meta( get_the_ID(), 'percorrenza2', true );
					if ( $valore != '' ) {
						?>
                        <li class="kml-metadata-div">
                            <b><?= __( 'Tempi di percorrenza (amatore)', 'wpkml' ) ?></b>: <?= $valore ?></li>
						<?php
					}

					$valore = get_post_meta( get_the_ID(), 'kml_kml', true );
					if ( $valore != '' ) {
						?>
                        <li class="kml-metadata-div-2"><a class="btn btn-primary" href="<?= $valore['url'] ?>"
                                                          target="_blank"><?= __( 'Scarica itinerario per cellulare *', 'wpkml' ) ?></a>
                        </li>
						<?php
					}

					$valore = get_post_meta( get_the_ID(), 'kml_gpx', true );
					if ( $valore != '' ) {
						?>
                        <li class="kml-metadata-div-2"><a class="btn btn-primary" href="<?= $valore['url'] ?>"
                                                          target="_blank"><?= __( 'Scarica itinerario per GPS *', 'wpkml' ) ?></a>
                        </li>
						<?php
					}

					$valore = get_post_meta( get_the_ID(), 'kml_scheda', true );
					if ( $valore != '' ) {
						?>
                        <li class="kml-metadata-div-2"><a class="btn btn-primary" href="<?= $valore['url'] ?>"
                                                          target="_blank"><?= __( 'Scarica scheda stampabile', 'wpkml' ) ?></a>
                        </li>
						<?php
					}
					?>

                    <small>*: per aprire un itinerario per cellulare dovete aver installato sul dispositivo un'applicazione adatta ad aprire file kml, ad esempio <a href="https://www.google.com/intl/it/earth/">Google Earth</a>.<br>I file GPX devono essere installati sul gps utilizzando le applicazioni fornite dal produttore.</small>.
                </ul>
            </div>

            </div>
            <div class="row" style="margin-top: 40px;">
				<?php $kml_hpicture = get_post_meta( get_the_ID(), 'kml_hpicture', true );
				if ( $kml_hpicture != '' ) {
					?>
                    <div class="col-sm-12 col-md-8 col-lg-8">
						<?= __( 'Profilo altimetrico', 'wpkml' ) ?>:
                        <img src="<?= $kml_hpicture['url'] ?>" style="max-width: 100%;"/>
                    </div>
					<?php

				}
				?>

            </div>

            <div class="row" style="margin-top: 40px;">
                <div class="col-sm-8 contenuto--itinerario"><?= $content ?><br></div>
            </div>

            <div class="row" style="margin-top: 40px; margin-bottom: 40px;">
                <div class=" col-sm-12 kml-pictures"><?php


					$pictures = get_post_meta( get_the_ID(), 'kml_pictures', true );
					if ( is_array( $pictures ) ) {
						?>
                        <div class="slider1"><?php
							foreach ( $pictures as $p ) {
								?>
                                <div class="slide"><a href="<?= $p['url'] ?>" data-lightbox="kml">
                                        <div style="background-image:url(<?= $p['url'] ?>);background-size:cover;width:300px;height:300px;display:inline-block;"></div>
                                    </a></div>
								<?php

							}
							?></div>
                        <script>
                            $(document).ready(function () {
                                $('.slider1').bxSlider({
                                    slideWidth: 300,
                                    minSlides: 2,
                                    maxSlides: 6,
                                    slideMargin: 10
                                });
                            });
                        </script>
						<?php
					} else {
						?><?= __( 'Non ci sono foto di questo percorso.', 'wpkml' ) ?><?php

					}

					?></div>

            </div> <!--row-->

			<?php


			return '';
		};

		if ( $type == 'poi' ) {
			$lat       = get_post_meta( get_the_ID(), 'lat', true );
			$lng       = get_post_meta( get_the_ID(), 'lng', true );
			$id        = get_the_id();
			$permalink = get_permalink( $id );
			$title     = get_the_title( $id );

			$icon = '';

			$tassonomie = get_the_terms( $id, 'poitype' );
			if ( is_array( $tassonomie ) ) {
				/// devo capire quali tassonomie sono associate
				foreach ( $tassonomie as $tt ) {
					$ttid  = $tt->term_id;
					$imgar = get_term_meta( $ttid, 'icon', true );
					if ( is_array( $imgar ) ) {
						$icon = $imgar['url'];
					}
				}
			}

			?>
            <div class="col-sm-12">
                <h2 id="page-title"><?php the_title(); ?></h2>
            </div>
            <div class="col-sm-12 col-md-8 col-lg-8">
                <div id="mapid"></div>
                <script>
                    var mymap = L.map('mapid');

                    var id = "<?=$id ?>";

                    L.tileLayer('<?=$tileset_url ?>', {
                        attribution: 'Test',
                        maxZoom: 16,
                        minZoom: 9
                    }).addTo(mymap);

                    poi = [];
                    poicon = "<?=$icon ?>";

                    if (poicon != "") {
                        console.log("Icona custom");
                        var myIcon = L.icon({
                            iconUrl: poicon,
                            //iconRetinaUrl: 'my-icon@2x.png',
                            //iconSize: [25, 41],
                            iconAnchor: [12, 41],
                            //popupAnchor: [12, 0],
                            //shadowUrl: 'my-icon-shadow.png',
                            //shadowRetinaUrl: 'my-icon-shadow@2x.png',
                            //shadowSize: [68, 95],
                            //shadowAnchor: [22, 94]
                        });
                        var options = {icon: myIcon};
                    } else {
                        console.log("Icona normale");
                        var options = {};
                    }


                    poi[id] = L.marker([<?=$lat?>,<?=$lng?>], options).addTo(mymap);

                    mymap.setView([<?=$lat?>,<?=$lng?>], 13);

                    mymap.on('dragend', redrawPOIs);
                    mymap.on('zoomend', redrawPOIs);

                    loadedpois = [id];
                    poi[id].bindPopup("<a href='<?=$permalink ?>'><b><?=$title ?></b></a>").openPopup();


                    console.log(loadedpois);

                    function redrawPOIs(arg) {
                        console.log(mymap.getBounds());

                        jQuery.post(
                            "<?=admin_url( 'admin-ajax.php' ) ?>",
                            {
                                'action': 'get_pois',
                                'data': {
                                    west: mymap.getBounds().getWest(),
                                    south: mymap.getBounds().getSouth(),
                                    east: mymap.getBounds().getEast(),
                                    north: mymap.getBounds().getNorth(),
                                    lang: "<?=ICL_LANGUAGE_CODE ?>",

                                }
                            },
                            function (response) {

                                res_array = JSON.parse(response);
                                console.log(res_array);
                                for (var i = 0; i < res_array.length; i++) {

                                    console.log(res_array[i]);

                                    newpoi = res_array[i];
                                    if (loadedpois.indexOf(newpoi.id) > -1) {
                                        console.log("Già caricato.");
                                    } else {
                                        console.log("Carico!");
                                        console.log(newpoi);
                                        var poicon = newpoi.icon;
                                        if (poicon != "") {
                                            console.log("Icona custom");
                                            var myIcon = L.icon({
                                                iconUrl: poicon,
                                                //iconRetinaUrl: 'my-icon@2x.png',
                                                //iconSize: [25, 41],
                                                iconAnchor: [12, 41],
                                                //popupAnchor: [12, 0],
                                                //shadowUrl: 'my-icon-shadow.png',
                                                //shadowRetinaUrl: 'my-icon-shadow@2x.png',
                                                //shadowSize: [68, 95],
                                                //shadowAnchor: [22, 94]
                                            });
                                            var options = {icon: myIcon};
                                        } else {
                                            console.log("Icona normale");
                                            var options = {};
                                        }
                                        poi[newpoi.id] = L.marker([newpoi.lat, newpoi.lng], options).addTo(mymap);
                                        poi[newpoi.id].bindPopup("<a href='" + newpoi.permalink + "'><b>" + newpoi.title + "</b></a>");
                                        loadedpois.push(newpoi.id);
                                    }

                                }


                            }
                        );

                    }


                    redrawPOIs();

                </script>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4 informazioni--poi">
				<?php
				$poi_picture = get_post_meta( get_the_ID(), 'poi_picture', true );
				if ( $poi_picture != '' ) {
					?>

                    <img style="max-width: 100%;" src="<?= $poi_picture['url'] ?>"/>

					<?php

				}
				?>
                <ul>
					<?php
					/*$valore = get_post_meta(get_the_ID(), 'lat', true);
					if ($valore != '0') {
						?>
						<li class="kml-metadata-div"><b><?= __('Latitudine', 'wpkml') ?></b>: <?= $valore ?></li>
						<?php
					}
					$valore = get_post_meta(get_the_ID(), 'lng', true);
					if ($valore != '0') {
						?>
						<li class="kml-metadata-div"><b><?= __('Longitudine', 'wpkml') ?></b>: <?= $valore ?></li>
						<?php
					}*/
					$valore = get_post_meta( get_the_ID(), 'poi_phone', true );
					if ( $valore != '' ) {
						?>
                        <li class="kml-metadata-div"><b><?= __( 'Telefono', 'wpkml' ) ?></b>: <a
                                    href="tel:<?= $valore ?>"><?= $valore ?></a></li>
						<?php
					}
					$valore = get_post_meta( get_the_ID(), 'poi_email', true );
					if ( $valore != '' ) {
						?>
                        <li class="kml-metadata-div"><b><?= __( 'Indirizzo e-mail', 'wpkml' ) ?></b>: <a
                                    href="mailto:<?= $valore ?>"><?= $valore ?></a></li>
						<?php
					}
					$valore = get_post_meta( get_the_ID(), 'poi_website', true );
					if ( $valore != '' ) {
						?>
                        <li class="kml-metadata-div"><b><?= __( 'Sito web', 'wpkml' ) ?></b>: <a
                                    href="<?= $valore ?>">Visita sito web</a></li>
						<?php
					}
					$valore = get_post_meta( get_the_ID(), 'poi_times', true );
					if ( $valore != '' ) {
						?>
                        <li class="kml-metadata-div">
                            <b><?= __( 'Orari di apertura', 'wpkml' ) ?></b>: <?= nl2br( $valore ) ?>
                        </li>
						<?php
					}
					?>
                </ul>
            </div>


            </div>
            <div class="row" style="margin-top: 40px;">
                <div class="col-sm-8"><?= $content ?></div>
            </div>
            <div class="row" style="margin-top: 40px; margin-bottom: 40px;">
            </div><?php
			return '';
		}
	}

	// default
	return $content;
}

// per redirect 302

add_action( 'template_redirect', 'callback_function_name' );
function callback_function_name() {
	global $post;
	if ( is_singular( 'poi' ) ) {
		$directlink = strtolower( get_post_meta( get_the_ID(), 'poi_directlink', true ) );
		if ( $directlink != '' ) {
			if ( substr( $directlink, 0, 7 ) == 'http://' ) {
				header( "Location: $directlink" );
			} else {
				if ( substr( $directlink, 0, 8 ) == 'https://' ) {
					header( "Location: $directlink" );
				} else {
					header( "Location: http://$directlink" );
				}
			}
			exit;
		}
	}
}

add_action( 'wp_ajax_get_pois', 'get_pois' );
add_action( 'wp_ajax_nopriv_get_pois', 'get_pois' );

function get_pois() {
	global $wpdb;

	$north = floatval( $_POST['data']['north'] );
	$south = floatval( $_POST['data']['south'] );
	$east  = floatval( $_POST['data']['east'] );
	$west  = floatval( $_POST['data']['west'] );

	$query = " SELECT `ID` FROM $wpdb->posts WHERE `post_type` = 'poi'; ";
	$res   = $wpdb->get_results( $query );

	$resarray = [];

	foreach ( $res as $row ) {
		$id = $row->ID;

		// modificare per farlo fare a wp?
		$latq = "SELECT `meta_value` FROM $wpdb->postmeta WHERE `post_id` = '$id' AND `meta_key` = 'lat'; ";
		$lat  = $wpdb->get_var( $latq );

		// modificare per farlo fare a wp?
		$lngq = "SELECT `meta_value` FROM $wpdb->postmeta WHERE `post_id` = '$id' AND `meta_key` = 'lng'; ";
		$lng  = $wpdb->get_var( $lngq );

		$icon = '';

		/// trovo quali tassonomie sono associate
		foreach ( get_the_terms( $id, 'poitype' ) as $tt ) {
			$ttid  = $tt->term_id;
			$imgar = get_term_meta( $ttid, 'icon', true );
			if ( is_array( $imgar ) ) {
				$icon = $imgar['url'];
			}
		}

		/// verifico la lingua
		$langquery = 'SELECT `language_code` FROM ' . $wpdb->prefix . "icl_translations WHERE `element_type` = 'post_poi' AND `element_id` = '" . $id . "'; ";
		$lang      = $wpdb->get_var( $langquery );

		if ( $lang == $_POST['data']['lang'] ) {
			if ( $lat >= $south && $lat <= $north && $lng >= $west && $lng <= $east ) {
				$resarray[] = [
					'id'        => $id,
					'lat'       => $lat,
					'lng'       => $lng,
					'permalink' => get_permalink( $id ),
					'title'     => get_the_title( $id ),
					'icon'      => $icon
				];
			} else {
				// non restituisco
			}
		}
	}

	echo json_encode( $resarray );

	wp_die();
}

function rewrite_flush() {
	create_post_type_kml_and_poi();
	flush_rewrite_rules();
}

register_activation_hook( __FILE__, 'rewrite_flush' );
